from __future__ import unicode_literals
from django.http import HttpResponse

from django.shortcuts import render

import json

from .models import Unit


def convertAPI(request):
    
    response = {}
    
    if not request.GET.has_key('value'):
        response['error_value'] = "variable 'value' not used"
    else:
        value = int(request.GET['value'])
        if(value < 0):
            response['error_value'] = "Usage: value=[non-negative number]"
        else:
            response['value'] = value 
            
    if not request.GET.has_key('from'):
        response['error_from'] = "variable 'from' not used"
    else:
        ufrom = request.GET['from']
        response['from'] = ufrom 
            
    if not request.GET.has_key('to'):
        response['error_to'] = "variable 'to' not used"
    else:
        uto = request.GET['to']
        response['to'] = uto
            
    response['t_oz'] = convert(value,ufrom,uto) 
    
    return HttpResponse(json.dumps(response))
    
def convert(value, ufrom, uto):
    unit_from = Unit.objects.get(name=ufrom)
    unit_to = Unit.objects.get(name=uto)
    
    value = unit_from.basis_factor * value
    value = value * (1.0 / unit_to.basis_factor)
    return value
    
  
    
# def convert(value, ufrom, uto):
#     unit_from = None
#     unit_from = Unit.objects.get(name=request.GET['from'])
#     unit_to = Unit.objects.get(name=request.GET['to'])
#     value_number = request.GET['value']
    
#     value_from = float(request.GET['value'])
#     value_basis = value_number * unit_from.basis_factor
#     value_to = value_basis * (1.0 / unit_to.basis_factor)
# def convert(value, ufrom, uto):
#      value = conversion[ufrom] * value  
#      value = value / conversion[uto] 
#      return value               

def index(request):
    return HttpResponse(request, 'index.html', {})
    
