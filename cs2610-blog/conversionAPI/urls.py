#Conversion API's urls
from django.conf.urls import url

from . import views

app_name = 'conversionAPI'
urlpatterns = [
    url(r'^convertAPI$', views.convertAPI, name='conversion'),
    url(r'^$', views.index, name='index'),
]