#conversionAPI's model
from __future__ import unicode_literals

from django.db import models

# conversion = {'stone' : 6350.2932, 'lbs' : 453.59237, 'kg' : 1000 , 't_oz' : 31.1035} 

# conversion['gram'] = 1
# all to grams

class Unit(models.Model):
    name = models.CharField(max_length=64)
    # Type = models.ForeignKey(name, on_delete=models.CASCADE)
    basis_factor=models.FloatField()
    
    def __str__(self):
        return "[%s] (%s)" % (self.name, self.basis_factor)

# self.Type.name,