from django.shortcuts import get_object_or_404,render
from django.views import generic
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
import datetime

from .models import Blog, Comments
    
def mainView(request):
    template_name = 'blog/main.html'
    return render(request,template_name,{'time': datetime.datetime.now()})
    
def bioView(request): 
    template_name = 'blog/bio.html'
    return render(request,template_name,{'time': datetime.datetime.now()})
    
def techView(request):
    template_name = 'blog/tech.html'
    return render(request,template_name,{'time': datetime.datetime.now()})
    
class homepage(generic.ListView):
    model = Blog
    template_name = 'blog/homepage.html'
    
    def get_queryset(self):
        return Blog.objects.order_by('-post_date')[:3]

class archive(generic.ListView):
    model = Blog
    template_name = 'blog/archive.html'
    
    def get_queryset(self):
        return Blog.objects.order_by('-post_date')
        
class entry(generic.DetailView):
    model = Blog
    template_name = 'blog/entry.html'
        
def postComment(request, blog_id):
    # for k, v in request.POST.items():
    #     print k, "=>", v
        
    comment = Comments()
    b = get_object_or_404(Blog, pk=blog_id)
    try:
        comment.nickname = request.POST['nickname']
        comment.content_comment = request.POST['comment']
        comment.email_address = request.POST['email_address']
        comment.post_date = request.POST['post_date']
    except (KeyError, Comments.DoesNotExist):
        return render(request, 'blog/entry.html', {'comment': comment,'error_message': "error",})
    else:
        # comment.blog = b
        comment.save()
    return HttpResponseRedirect(reverse('blog:entry',args=(b.id,)))
    