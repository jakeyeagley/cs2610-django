from django.conf.urls import url 

from . import views

app_name = 'blog'
urlpatterns = [
    url(r'^$', views.mainView, name='main'),
    url(r'^main/$', views.mainView, name='main'),
    url(r'^bio/$', views.bioView, name='bio'),
    url(r'^tech/$', views.techView, name='tech'),
    url(r'^homepage/$', views.homepage.as_view(), name='homepage'),
    url(r'^entry/$', views.entry.as_view(), name='entry'),
    url(r'^archive/$',views.archive.as_view(), name='archive'),
    url(r'^(?P<pk>[0-9]+)/entry/$', views.entry.as_view(), name='entry'),
    url(r'^(?P<blog_id>[0-9]+)/comment/$', views.postComment, name='postComment'),   
    #url(r'^blog/entry/(?P<blog_id>[0-9]+)/$', views.entry.as_view(), name='entry'),
]