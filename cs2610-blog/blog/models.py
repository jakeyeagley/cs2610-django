from __future__ import unicode_literals

from django.db import models
from django import forms
from django.utils import timezone

class Blog(models.Model):
    title_blog = models.CharField(max_length=200)
    author_name = models.CharField(max_length=200)
    content_blog = models.TextField()
    post_date = models.DateTimeField('posted date')
    
    def __str__(self):
        return self.content_blog
        
    
class Comments(models.Model):
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
    nickname = models.CharField(max_length=200)
    email_address = models.CharField(max_length=200)
    content_comment = models.TextField()
    post_date = models.DateTimeField('posted date')
    
    def __str__(self):
        return self.content_comment

